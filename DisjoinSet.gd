# Based on https://github.com/andraantariksa/goeat/blob/master/lib/goeat/disjoinset.rb

class_name DisjoinSet

var size = 0
var items = {}

func union(root1, root2):
	if items[root2] < items[root1]:
		items[root1] = root2
	else:
		if items[root1] == items[root2]:
			items[root1] -= 1
		items[root2] = root1

func find(key):
	while true:
		if key in items.keys():
			if typeof(items[key]) == TYPE_INT:
				if items[key] <= 0:
					return key
			key = items[key]
		else:
			items[key] = -1
			return key

func split_sets():
	var sets = {}
	for j in items.keys():
		var root = find(j)
		if typeof(root) == TYPE_INT:
			if root <= 0:
				continue
		if root in sets.keys():
			var list = sets[root]
			list.append(j)
			sets[root] = list
		else:
			sets[root] = [j]
	return sets