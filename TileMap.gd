extends TileMap

const WIDTH = 55
const HEIGHT = 37
const AREA = WIDTH * HEIGHT
const CENTER_POINT = [WIDTH / 2, HEIGHT / 2]
const FLOOR_PERCENTAGE = 0.40 # 40 %
const FLOOR = 0
const WALL = 1

var DisjoinSet

func all_tiles_to(thing):
	for y in range(0, HEIGHT):
		for x in range(0, WIDTH):
			set_cell(x, y, thing)

func random_open_space():
	var total_open_space = int(AREA * FLOOR_PERCENTAGE)
	while total_open_space > 0:
		# Random row between 1 to (HEIGHT - 1)
		var random_row = randi() % (HEIGHT - 2) + 1
		if random_row == 0 || random_row == HEIGHT - 1:
			continue
		# Random columnbetween 1 to (WIDTH - 1)
		var random_column = randi() % (WIDTH - 2) + 1
		if get_cell(random_column, random_row) == WALL:
			set_cell(random_column, random_row, FLOOR)
			total_open_space -= 1

func count_adjacent_wall(row, column):
	var total = 0
	for another_row in [-1, 0, 1]:
		for another_column in [-1, 0, 1]:
			# Skip self
			if another_row == 0 && another_column == 0:
				continue
			var next_column = column + another_column
			var next_row = row + another_row
			if get_cell(next_column, next_row) != FLOOR:
				total += 1
	return total

func clear_random_open_space():
	for row_i in range(1, HEIGHT - 1):
		for column_i in range(1, WIDTH - 1):
			#yield(get_tree().create_timer(0.1), "timeout")
			var total_wall = count_adjacent_wall(row_i, column_i)
			if get_cell(column_i, row_i) == FLOOR:
				if total_wall > 5:
					set_cell(column_i, row_i, WALL)
			elif total_wall < 4:
				set_cell(column_i, row_i, FLOOR)

func union_adjacent_square(row_i, column_i):
	var location = [row_i, column_i]

	for another_row in [-1, 0]:
		for another_column in [-1, 0]:
			# No diagonal -1,-1   -1,1   1,-1   1,1
			if abs(another_row) + abs(another_column) == 2:
				continue

			var n_location = [row_i + another_row, column_i + another_column]

			if get_cell(n_location[1], n_location[0]) == FLOOR:
				var root1 = DisjoinSet.find(location)
				var root2 = DisjoinSet.find(n_location)
				if root1 != root2:
					DisjoinSet.union(root1, root2)

func populate_caves():
	for row_i in range(1, HEIGHT - 1):
		for column_i in range(1, WIDTH - 1):
			if get_cell(column_i, row_i) == FLOOR:
				union_adjacent_square(row_i, column_i)
	return DisjoinSet.split_sets()

func get_tunnel_direction(point1, point2):
	var horizontal_direction
	var vertical_direction

	if point1[0] < point2[0]:
		horizontal_direction = 1
	elif point1[0] > point2[0]:
		horizontal_direction = -1
	else:
		horizontal_direction = 0

	if point1[1] < point2[1]:
		vertical_direction = 1
	elif point1[1] > point2[1]:
		vertical_direction = -1
	else:
		vertical_direction = 0

	return [horizontal_direction, vertical_direction]

func stop_drawing(current_point, next_point, center_point, direction):
	if DisjoinSet.find(next_point) == DisjoinSet.find(center_point) && get_cell(next_point[1] - direction[1], next_point[0]) == FLOOR && get_cell(next_point[1], next_point[0] - direction[0]) == FLOOR:
		return true

	if DisjoinSet.find(current_point) != DisjoinSet.find(next_point) && get_cell(next_point[1], next_point[0]) == FLOOR && get_cell(next_point[1] - direction[1], next_point[0]) == FLOOR && get_cell(next_point[1], next_point[0] - direction[0]) == FLOOR:
		return true
	else:
		return false

func join_points(current_point):
	var next_point = current_point

	while true:
		var direction = get_tunnel_direction(current_point, CENTER_POINT)
		var move = randi() % 3

		if move == 0:
			next_point = [current_point[0] + direction[0], current_point[1]]
		elif move == 1:
			next_point = [current_point[0], current_point[1] + direction[1]]
		else:
			next_point = [current_point[0] + direction[0], current_point[1] + direction[1]]

		if stop_drawing(current_point, next_point, CENTER_POINT, direction):
                return

		var root1 = DisjoinSet.find(next_point)
		var root2 = DisjoinSet.find(current_point)

		if root1 != root2:
			DisjoinSet.union(root1, root2)

		if get_cell(next_point[1] - direction[1], next_point[0]) == WALL:
			set_cell(next_point[1] - direction[1], next_point[0], FLOOR)
		if get_cell(next_point[1], next_point[0] - direction[0]) == WALL:
			set_cell(next_point[1], next_point[0] - direction[0], FLOOR)
		if get_cell(next_point[0], next_point[1]) == WALL:
			set_cell(next_point[1], next_point[0], FLOOR)

		current_point = next_point

func join_rooms():
	var all_caves = populate_caves()
	for cave in all_caves.keys():
		join_points(all_caves[cave][0])

func generate():
	all_tiles_to(WALL)
	random_open_space()
	clear_random_open_space()
	join_rooms()

func _ready():
	DisjoinSet = load("res://DisjoinSet.gd").new()
	generate()