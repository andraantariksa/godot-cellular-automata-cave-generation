extends Button

func _pressed():
	var tile_map = get_node("/root/Main/TileMap")
	tile_map.all_tiles_to(tile_map.WALL)
	tile_map.DisjoinSet = load("res://DisjoinSet.gd").new()